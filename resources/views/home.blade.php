@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Bem-vindo</div>

                <div class="panel-body">
                    <b>Olá {{ Auth::user()->name }}!</b><br>
                    @if(!empty($reservation))
                    <p>Abaixo as suas reservas</p>
                    
                    <table class="table table-striped">
                <thead style="font-weight: bold; background: #dedede">
                    <tr>
                        <td>Id</td>
                        <td>Sala</td>
                        <td>Data de reserva</td>       
                        <td>Período</td>
                    </tr>
                </thead>

                @foreach($reservation as $val)
                <tbody>
                    <tr>
                        <td>{{ $val->id }}</td>
                        <td>{{ $val->room->title }}</td>
                        <td><i class="fa fa-calendar"></i> {{ Carbon\Carbon::parse($val->date_reservation)->format('d-m-Y ') }}</td>

                        <td>{{ $val->start_period }} horas - {{ $val->finish_period }} horas</td>
                        
                      
                    </tr>
                </tbody>
                @endforeach

            </table>
            {!! $reservation->render() !!}
            
            @endif
                 
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
