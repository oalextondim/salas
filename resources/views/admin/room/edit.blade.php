@extends('layouts.app')

@section('content')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.rooms.index')}}">Produtos</a></li>
        <li class="active"><a href="#">Edição</a></li>

    </ol>
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div  style="padding: 15px">

                    <h3>Editar sala</h3>


                    {!! Form::model($room, ['route' => ['admin.rooms.update',$room->id] ,'method' => 'put']) !!}


                    @include('admin.room._form',['btnSubmitName' => 'Salvar'])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div><!-- container -->
@endsection
