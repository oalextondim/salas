@extends('layouts.app')

@section('content')
<div class="container">




    <div class="row">
        <div class="col-sm-10">
            <h1>Salas</h1>
        </div>
        <div class="col-sm-2">
            <a href="{{ route('admin.rooms.create')}}" class="btn btn-primary pull-right">
                <i class="fa fa-plus" aria-hidden="true"></i> Novo</a>
        </div>
    </div>

    @include('shared.message')

    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><h4>Listagem</h4></div>
        <div class="panel-body">
            <!-- Table -->
            <table class="table table-striped">
                <thead style="font-weight: bold; background: #dedede">
                    <tr>
                        <td>COD</td>
                        <td>Título</td>
                        <td width="1%" nowrap>Ação</td>
                    </tr>
                </thead>

                @foreach($rooms as $val)
                <tbody>
                    <tr>
                        <td>{{ $val->cod }}</td>
                        <td>{{ $val->title }}</td>
                        <td width="1%" nowrap>
                            <a href="{{ route('admin.rooms.edit', ['id' => $val->id] )}}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a href="{{ route('admin.rooms.destroy',['id' => $val->id]) }}" class="btn btn-danger btn-sm "><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                </tbody>
                @endforeach

            </table>
            {!! $rooms->render() !!}
        </div>
    </div>
</div>

@include('shared.modal-delete', ['modalId' => 'modalDelete'])

@endsection
