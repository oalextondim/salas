@extends('layouts.app')

@section('content')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.rooms.index')}}">Salas</a></li>
        <li class="active"><a href="#">Cadastro</a></li>

    </ol>
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div  style="padding: 15px">

                    <h3>Cadastro de salas</h3>

                    {!! Form::open(['route' => 'admin.rooms.store', 'method' => 'post']) !!}

                    @include('admin.room._form',['btnSubmitName' => 'Salvar'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

</div><!-- container -->
@endsection
