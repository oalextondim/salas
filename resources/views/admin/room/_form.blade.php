@include('errors._error_field')

<?php $errorClass = $errors->first('name') ? ['class' => 'validate invalid'] : []; ?>

<div class="row">
    <div class="col-sm-2">
          {!! Form::label('cod', 'Código') !!}
            {!! Form::text('cod', $room->cod, array_merge(['class' => 'form-control input-sm', 'required' => 'required'])) !!}
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('title', 'Nome') !!}
            {!! Form::text('title', $room->title, array_merge(['class' => 'form-control  input-sm', 'required' => 'required'])) !!}
        </div>
    </div>
   
      </div>


<div class="row">

    <div class="col-sm-12">
        <br/>
        {!! Form::submit($btnSubmitName, ['class' => 'btn btn-primary btn-lg waves-effect right']) !!}
        <a href="{{ route('admin.rooms.index')}}" class="btn btn-danger btn-lg waves-effect right">Cancelar</a>
    </div>
</div><!-- row -->