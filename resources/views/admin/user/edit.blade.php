@extends('layouts.app')

@section('content')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.users.index')}}">Usuários</a></li>
        <li class="active"><a href="#">Edição</a></li>

    </ol>


    <h3>Editar usuário</h3>
    <hr/>
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-body">
                {!! Form::model($users, ['route' => ['admin.users.update',$users->id] ,'method' => 'put']) !!}


                @include('admin.user._form',['btnSubmitName' => 'Salvar'])

                {!! Form::close() !!}

            </div></div></div>   
</div><!-- container -->
@endsection
