
@if (Session::has('success'))
<div class="alert alert-success">
    <strong>{{ Session::get('success') }}</strong>
</div>


@endif


@if ($errors->has('confirmPassword'))
<div class="alert alert-success">
    <strong>{{ $errors->first('confirmPassword') }}</strong>
</div>
@endif

@if ($errors->has('password'))
<div class="alert alert-success">
    <strong>{{ $errors->first('password') }}</strong>
</div>
@endif
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {{ csrf_field() }}


            {!! Form::label('name', 'Nome') !!}
            {!! Form::text('name', $users->name, array_merge(['class' => 'form-control', 'required' => 'required'])) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {!! Form::label('email', 'E-mail') !!}
            {!! Form::text('email', $users->email, array_merge(['class' => 'form-control', 'required' => 'required'])) !!}
        </div>
    </div>

</div>


<div class="row" >
    <div class="col-sm-6">
        @if(!empty($users->id))   
        <label class="checkbox-inline">
            {!! Form::checkbox('alterPass', '' , false, ['id' => 'alterPass']) !!} Alterar senha
        </label>
        @endif

        <div style="{!! (!empty($users->id)) ? 'display:none' : '' !!}"  id="pass">


            {!! Form::label('password', 'Senha', ['data-error' => $errors->first('password')]) !!}
            <input type="password" {!! (empty($users->id)) ? 'required' : '' !!}
            pattern="(?=.*\d)(?=.*[a-z]).{5,}" placeholder="Deve conter mais de 5 caracteres e um número"
            title="Senha deve conter mais de cinco caracteres e um número " size="20" 
            name="password"  class="form-control" onchange="confirmPassword.pattern = this.value;" id="password">

            {!! Form::label('confirmPassword', 'Confirme a senha', ['data-error' => $errors->first('password')]) !!}
            <input type="password" {!! (empty($users->id)) ? 'required' : '' !!} pattern="(?=.*\d)(?=.*[a-z]).{5,}"  class="form-control" 
            title="Senhas diferentes" size="20" placeholder="Repita senha" name="confirmPassword" id="confirmPassword">
        </div>
    </div>
</div>



<!-- row -->

<div class="row">

    <div class="col-sm-12">
        <br/>
        {!! Form::submit($btnSubmitName, ['class' => 'btn btn-primary btn-lg waves-effect right']) !!}

        <a href="{{ route('admin.users.index')}}" class="btn btn-danger btn-lg waves-effect right">Cancelar</a>
    </div>
</div><!-- row -->


@section('javascript-footer')
<script type="text/javascript">
    $(document).ready(function () {
        $('#alterPass').click(function () {
            var elPassword = $("#password"),
                    elConfirmPassword = $("#confirmPassword"),
                    elPass = $("#pass");

            if ($(this).is(':checked') == true) {
                elPass.show();

                elConfirmPassword.prop('required', true);
                elPassword.prop('required', true);
            } else {
                elPassword.prop('required', false).val("");
                elConfirmPassword.prop('required', false).val("");
                elPass.hide();
            }
        });

    });
</script>
@parent
@endsection