@extends('layouts.app')

@section('content')
<div class="container">




    <div class="row">
        <div class="col-sm-10">
            <h1>Todas Reservas</h1>
        </div>
        <div class="col-sm-2">
            <a href="{{ route('admin.room-reservations.create')}}" class="btn btn-primary pull-right">
                <i class="fa fa-plus" aria-hidden="true"></i> Novo</a>
        </div>
    </div>

    @include('shared.message')

    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><h4>Listagem</h4></div>
        <div class="panel-body">
            <!-- Table -->
            <table class="table table-striped">
                <thead style="font-weight: bold; background: #dedede">
                    <tr>
                        <td>Id</td>
                        <td>Sala</td>
                               <td>Período ocupada</td>
                        <td>Usuário</td>
                        <td>Data de reserva</td>
                 
                        <td width="1%" nowrap>Ação</td>
                    </tr>
                </thead>

                @foreach($roomReservations as $val)
                <tbody>
                    <tr>
                        <td>{{ $val->id }}</td>
                        <td>{{ $val->room->title }}</td>
                        <td>{{ $val->start_period }} horas - {{ $val->finish_period }} horas</td>
                        <td>{{ $val->user->name }}</td>
                        <td>{{ Carbon\Carbon::parse($val->date_reservation)->format('d-m-Y ') }}</td>
                        
                        <td width="1%" nowrap>
                            @if( $val->user->id == Auth::user()->id)
                              <a href="{{ route('admin.room-reservations.destroy',['id' => $val->id]) }}" class="btn btn-danger btn-sm ">
                                <i class="fa fa-trash" aria-hidden="true"></i> Cancelar reserva</a>
                            @endif
                          
                        </td>
                    </tr>
                </tbody>
                @endforeach

            </table>
            {!! $roomReservations->render() !!}
        </div>
    </div>
</div>

@include('shared.modal-delete', ['modalId' => 'modalDelete'])

@endsection
