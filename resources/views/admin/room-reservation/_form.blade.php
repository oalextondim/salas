@include('errors._error_field')

<?php $errorClass = $errors->first('name') ? ['class' => 'validate invalid'] : []; ?>

    @include('shared.message')
<div class="row">
      <div class="col-md-3">
           <div class="form-group">
      
            {!! Form::label('room_id', 'Sala') !!}
            {!! Form::select('room_id', $rooms, $roomReservation->room_id,  array_merge(['class' => 'form-control', 'required' =>'required'])) !!}
        </div>
    </div>
    <div class="col-sm-2">
     <div class="form-group">
    
       
            {!! Form::label('date_reservation', 'Data de reserva', ['data-error' => $errors->first('date_reservation')]) !!}
            {!! Form::text('date_reservation', 
            empty($roomReservation->date_reservation)? '' : Carbon\Carbon::parse($roomReservation->date_reservation)->format('d-m-Y ')
            , array_merge(['class' => 'form-control datetimepicker date'], $errorClass)) !!}
        </div>
    </div>
      <div class="col-md-3">
    <div class="form-group">
      
            {!! Form::label('start_period', 'Periodo de uma horas') !!}
            {!! Form::select('start_period', [''=>'Selecione',
            '8'=>'8 horas','9'=>'9 horas','10'=>'10 horas','11'=>'11 horas','12'=>'12 horas','13'=>'13 horas',
            '14'=>'14 horas','15'=>'15 horas','16'=>'16 horas','17'=>'17 horas','18'=>'18 horas'], null,  array_merge(['class' => 'form-control', 'required' =>'required'])) !!}
        </div>
    </div>

</div>


<div class="row">

    <div class="col-sm-12">
        <br/>
        {!! Form::submit($btnSubmitName, ['class' => 'btn btn-primary btn-lg waves-effect right']) !!}
        <a href="{{ route('admin.room-reservations.index')}}" class="btn btn-danger btn-lg waves-effect right">Cancelar</a>
    </div>
</div><!-- row -->