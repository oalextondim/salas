<div class="modal fade" tabindex="-1" role="dialog" id="{{$modalId}}">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
          <h2>
              Deseja excluir ...
          </h2>
          
             
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <!--
        <button type="button" class="btn btn-danger" id="btnDeleteModal">Excluir</button>
        -->
        <a href="#" class="btn btn-danger" id="btnDeleteModal" data-csrf-token="{{ csrf_token() }}">Excluir <i class="fa fa-trash" aria-hidden="true"></i></a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->