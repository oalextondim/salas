<div class="modal fade" tabindex="-1" role="dialog" id="{{$modalId}}">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
          <h2>Alterar Status</h2>
          
          
          <select class="form-control" id="situation_id" name="situation_id">
              
                    @foreach($situations as $situations)
                    <option value="{{ $situations->id }}">{{ $situations->title }}</option>
                    @endforeach



                </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <!--
        <button type="button" class="btn btn-danger" id="btnDeleteModal">Excluir</button>
        -->
        <button type="button"  class="btn btn-success" id="btneditModal" data-csrf-token="{{ csrf_token() }}">
            Salvar <i class="fa fa-check" aria-hidden="true"></i></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->