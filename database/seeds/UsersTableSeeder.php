<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()//cria apenas um user
    {
        factory(App\User::class,1)
                //->states('admin')->create([
                ->create([
                    'name' => 'Administrador',
                    'email' => 'teste@teste.com'
                    ]);        
                
    }
}