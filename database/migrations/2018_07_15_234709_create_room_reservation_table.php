<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomReservationTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('room_reservations', function(Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('room_id')->unsigned();
            $table->foreign('room_id')->references('id')->on('rooms');
            
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->date('date_reservation');
            $table->date('start_period');
            $table->date('finish_period');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('room_reservations');
    }

}
