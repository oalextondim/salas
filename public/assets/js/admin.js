"use strict";

var App = function () {
    
    var handleInit = function () {
        var deviceAgent = navigator.userAgent.toLowerCase();
        if (deviceAgent.match(/(iphone|ipod|ipad)/)) {
//            $(document).on('focus', 'input, textarea', function () {
//                $('.header').hide();
//                $('.footer').hide();
//            });
        }
    }

    return {

        init: function () {
            App.inputMask();
        },

        inputMask: function(){
            $(".date").mask("99-99-9999");
            $('.mask_phone_with_ddd').mask('(00) 0000-00000');
            $(".mask_cpf").mask('000.000.000-00');
            $(".cep").mask('00000-000');
            //$('.mask_money').mask('000000000000000,00', {reverse: true});
            $('.mask_money').mask('#.##0,00', {reverse: true});
//            $(function(){
//                $(".valor").maskMoney(); // Somente com isso o nosso campo já está usando a máscara de valor monetário
//            });
        },

        blockUI: function (el, message, centerY) {
            var el = jQuery(el),
                message = message != undefined ? message : '';
            if (el.height() <= 400) {
                centerY = true;
            }
            message = '<i class="fa fa-spinner fa-spin"></i> ' + message;
            
            el.block({
                //message: '<img src="/assets-default/img/ajax-loading.gif" align="">', //
                message: message, //
                centerY: centerY != undefined ? centerY : true,
                css: {
                    //top: '10%',
                    top: '113px',
                    border: 'none',
                    padding: '2px',
                    backgroundColor: 'none'
                },
                overlayCSS: {
                    backgroundColor: '#000',
                    //opacity: 0.05,
                    opacity: 0.13,
                    cursor: 'wait'
                }
            });
        },

        // wrapper function to  un-block element(finish loading)
        unblockUI: function (el, clean) {
            jQuery(el).unblock({
                onUnblock: function () {
                    jQuery(el).css('position', '');
                    jQuery(el).css('zoom', '');
                }
            });
        }
    };
}();


$(function(){
    $(document).ready(function () {
        
        App.init();
 $('.select2').select2();
        $(".btn-smart-delete").on("click", function () {
            var $elModal = $('#modalDelete'),
                    $elBtn = $('#modalDelete').find('#btnDeleteModal'),
                    route = $(this).data('route');

            $elBtn.attr('data-route', route);
            $elModal.modal('show');
            //console.log(targetUrl);
            //console.log(route);
            //
        });

        $("#btnDeleteModal").on("click", function () {
            var route = $(this).data('route'),
                    token = $(this).data('csrf-token');
    //                    console.log(route);
    //                    console.log(token);
            $.ajax({
                url: route,
                type: 'post',
                data: {_method: 'delete', _token: token},
                success: function (result) {

                    location.reload();
                }
            });

        });


        // tratamento para bloquear quando submit
        $( ".ss-block" ).submit(function( event ) {
            $.blockUI({ message: 'Aguarde...' }); 
        });

        $( ".datepicker" ).datepicker({
               inline: true,
            dateFormat: 'dd-mm-yy',
            dayNames: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
            dayNamesMin: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
            monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            nextText: "Próximo",
            prevText: "Anterior"
        });
    }); // function

});