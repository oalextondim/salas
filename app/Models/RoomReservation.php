<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomReservation extends Model {

    protected $fillable = [
        'room_id',
        'user_id',
        'date_reservation',
        'start_period',
        'finish_period'
    ];

    public function room() {
        return $this->belongsTo('App\Models\Room', 'room_id', 'id');
    }

    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
