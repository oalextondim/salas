<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Room;
use App\Models\RoomReservation;

class RoomsController extends Controller {

    private $room;
    private $roomReservation;

    public function __construct(Room $room, RoomReservation $roomReservation) {
        $this->room = $room;
        $this->roomReservation = $roomReservation;
    }

    public function index() {

        $rooms = $this->room->paginate(5);

        return view('admin.room.index', compact('rooms'));
    }

    public function store(Request $request) {

        $input = $request->all();

        Room::create($input);
        return redirect('admin/rooms');
    }

    public function create() {

        $room = new Room();

        return view('admin.room.create', compact('room'));
    }

    public function edit($id) {
        $room = $this->room->find($id);

        return view('admin.room.edit', compact('room'));
    }

    public function update($id, Request $request) {

        $room = $this->room->find($id);

        $inputs = $request->all();

        $room->update($inputs);
        $request->session()->flash('success', 'Alterado com sucesso.');

        return redirect('admin/rooms');
    }

    public function destroy($id, Request $request) {

        $roomReservations = $this->roomReservation->where('user_id', $id)->count();

        if ($roomReservations != 0) {
            $request->session()->flash('danger', 'Não é possivel excluir, existem registros associados a essa sala.');
            return redirect()->back();
        } else {
            $room = $this->room->find($id)->delete();

            $request->session()->flash('success', 'Excluido com sucesso.');
            return redirect()->back();
        }
    }

}
