<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RoomReservation;

use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
      private $roomReservation;
    public function __construct(RoomReservation $roomReservation)
    {
        $this->middleware('auth');
        $this->roomReservation = $roomReservation;
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservation = $this->roomReservation
                ->where('user_id', Auth::user()->id)->paginate(5);
        
        return view('home',compact('reservation'));
    }
}
