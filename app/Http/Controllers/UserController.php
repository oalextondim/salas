<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\RoomReservation;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller {

    private $users;
    private $roomReservation;

    public function __construct(User $users, RoomReservation $roomReservation) {
        $this->users = $users;
        $this->roomReservation = $roomReservation;
    }

    public function index() {

        $users = $this->users->paginate(5);

        return view('admin.user.index', compact('users'));
    }

    protected function userExist(Request $request) {

        $this->users
                ->where('email', $request['email'])
                ->first();

        if ($this->users->where('email', $request['email'])
                        ->first() != null) {
            return true;
        }

        return false;
    }

    public function create() {
        $users = new User();

        return view('admin.user.create', compact('users'));
    }

    public function store(Request $request) {
        $v = Validator::make($request->all(), [
                    'password' => 'required|min:5',
                    'confirmPassword' => 'required|same:password'
                        ], [], [
                    'password' => 'Senha',
                    'confirmPassword' => 'Confirmação de senha',
        ]);

        if ($v->fails()) {

            return redirect()->back()->withErrors($v);
        }

        $this->userExist($request);

        if ($this->userExist($request) == true) {

            return redirect()->back()->withErrors(
                            $request->session()->flash('success', 'Usuário já cadastrado! Acesse edição de usuários')
            );
        }

        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
        ]);


        return redirect('admin/users');
    }

    public function edit($id) {
        $users = $this->users->find($id);
        return view('admin.user.edit', compact('users'));
    }

    public function update($id, Request $request) {

        $inputs = $request->all();

        $user = $this->users->find($id);

        if (empty($inputs['password']))
            unset($inputs['password']);
        else
            $inputs['password'] = bcrypt($inputs['password']);

        $user->update($inputs);


        return redirect('admin/users');
    }

    public function destroy($id, Request $request) {

        $roomReservations = $this->roomReservation->where('user_id', $id)->count();

        if ($roomReservations != 0) {
            $request->session()->flash('danger', 'Não é possivel excluir, existem registros associados a essa pessoa.');
            return redirect()->back();
        } else {
            $users = $this->users->find($id)->delete();
            $request->session()->flash('success', 'Excluido com sucesso.');
            return redirect()->back();
            //return ['ok' => true];
        }
    }

}
