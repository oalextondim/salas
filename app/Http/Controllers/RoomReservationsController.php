<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RoomReservationsRequest;
use App\Models\RoomReservation;
use App\Models\Room;
use Auth;

class RoomReservationsController extends Controller {

    private $roomReservation;
    private $room;

    public function __construct(RoomReservation $roomReservation, Room $room) {

        $this->roomReservation = $roomReservation;
        $this->room = $room;
    }

    public function index() {

        $roomReservations = $this->roomReservation->with('room', 'user')->paginate(7);

        return view('admin.room-reservation.index', compact('roomReservations'));
    }

    public function store(RoomReservationsRequest $request) {

        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        $input['finish_period'] = $input['start_period'] + 1;

        $checkByUserAndPeriod = $this->roomReservation
                ->where('user_id', $input['user_id'])
                ->where('room_id', $input['room_id'])
                ->where('date_reservation', $input['date_reservation'])
                ->where('start_period', $input['start_period'])
                ->where('finish_period', $input['finish_period'])
                ->count();

        $checkByOtherUserAndPeriod = $this->roomReservation
                ->where('user_id', '!=', $input['user_id'])
                ->where('room_id', $input['room_id'])
                ->where('date_reservation', $input['date_reservation'])
                ->where('start_period', $input['start_period'])
                ->where('finish_period', $input['finish_period'])
                ->count();

        if ($checkByUserAndPeriod != 0) {
            $request->session()->flash('warning', 'Sala reservada nesse horário.');
            return redirect()->back();
        }
        if ($checkByOtherUserAndPeriod != 0) {
            $request->session()->flash('warning', 'Sala reservada nesse por outra pessoa nesse horário.');
            return redirect()->back();
        }

        RoomReservation::create($input);
        return redirect('admin/room-reservations');
    }

    public function create() {

        $roomReservation = new Room();
        $rooms = $this->room->get()->pluck('title', 'id');
        //dd($rooms);

        return view('admin.room-reservation.create', compact('roomReservation', 'rooms'));
    }

    public function destroy($id, Request $request) {


        $roomReservation = $this->roomReservation->find($id)->delete();

        $request->session()->flash('success', 'Reserva Cancalada.');
        return redirect()->back();
    }

}
