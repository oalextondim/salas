<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class RoomReservationsRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $this->sanitize();
        return [
                //
        ];
    }

    public function sanitize() {
        $inputs = $this->all();

        $inputs['date_reservation'] = Carbon::parse($inputs['date_reservation'])->format('Y-m-d');
        
        //$inputs['sale_value'] = str_replace(',', '.', str_replace('.', '', $inputs['sale_value']));

        $this->replace($inputs);
    }

}
