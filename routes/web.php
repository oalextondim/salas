<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::get('/', function () {
    //return view('welcome');
    return redirect('login/');
});

Auth::routes();

Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
    'middleware' => ['auth']
        ], function() {

    Route::get('/home', 'HomeController@index');
    
    Route::get('/users/destroy/{id}', ['as' => 'users.destroy', 'uses' => 'UserController@destroy']);
    Route::resource('users', 'UserController', [
        'only' => [
            'index', 'create', 'store', 'update', 'edit'//, 'destroy'
        ]
    ]);

    Route::get('/rooms/destroy/{id}', ['as' => 'rooms.destroy', 'uses' => 'RoomsController@destroy']);
    Route::resource('rooms', 'RoomsController', [
        'only' => [
            'index', 'create', 'store', 'update', 'edit'//, 'destroy'
        ]
    ]);
    
    Route::get('/room-reservations/destroy/{id}', ['as' => 'room-reservations.destroy', 'uses' => 'RoomReservationsController@destroy']);
    Route::resource('room-reservations', 'RoomReservationsController', [
        'only' => [
            'index', 'create', 'store', 'update', 'edit'//, 'destroy'
        ]
    ]);
});
