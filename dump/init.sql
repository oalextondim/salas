-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.6.17 - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para salas
CREATE DATABASE IF NOT EXISTS `salas` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `salas`;

-- Copiando estrutura para tabela salas.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela salas.migrations: ~4 rows (aproximadamente)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2018_07_15_233634_create_rooms_table', 2),
	(4, '2018_07_15_234709_create_room_reservation_table', 3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Copiando estrutura para tabela salas.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela salas.password_resets: ~0 rows (aproximadamente)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Copiando estrutura para tabela salas.rooms
CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cod` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela salas.rooms: ~2 rows (aproximadamente)
DELETE FROM `rooms`;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` (`id`, `title`, `cod`, `created_at`, `updated_at`) VALUES
	(1, 'sala 10', '1', '2018-07-15 23:42:03', '2018-07-15 23:43:45'),
	(3, 'sala 2', '2', '2018-07-16 01:31:47', '2018-07-16 01:31:47');
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;

-- Copiando estrutura para tabela salas.room_reservations
CREATE TABLE IF NOT EXISTS `room_reservations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `date_reservation` date NOT NULL,
  `start_period` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `finish_period` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `room_reservation_room_id_foreign` (`room_id`),
  KEY `room_reservation_user_id_foreign` (`user_id`),
  CONSTRAINT `room_reservation_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`),
  CONSTRAINT `room_reservation_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela salas.room_reservations: ~3 rows (aproximadamente)
DELETE FROM `room_reservations`;
/*!40000 ALTER TABLE `room_reservations` DISABLE KEYS */;
INSERT INTO `room_reservations` (`id`, `room_id`, `user_id`, `date_reservation`, `start_period`, `finish_period`, `created_at`, `updated_at`) VALUES
	(6, 1, 1, '2018-07-01', '9', '10', '2018-07-16 01:12:57', '2018-07-16 01:12:57'),
	(8, 1, 3, '2018-07-01', '8', '9', '2018-07-16 01:41:31', '2018-07-16 01:41:31'),
	(9, 3, 3, '2018-07-16', '9', '10', '2018-07-16 01:47:20', '2018-07-16 01:47:20');
/*!40000 ALTER TABLE `room_reservations` ENABLE KEYS */;

-- Copiando estrutura para tabela salas.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela salas.users: ~2 rows (aproximadamente)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Administrador', 'teste@teste.com', '$2y$10$6pTXXvajs.sAHLT8OHLas.4ntLmeaeNr3523rSiWlyJwp9eOnMNBe', 'mLgTOvEaDtC1xHBKPYdKErRxSkyVnlXQLMOVJDZd26JkxYk2FeUtW9UaWYUg', '2018-07-15 22:18:57', '2018-07-16 02:05:27'),
	(3, 'joão', 'joao@joao.com', '$2y$10$3GpKoAQaxRmQZjPC3Vmi4.Yw6GCJFJtGcKH7VDkhxgcJu8t7lr5Jm', 'uAegSV52Nl2hPrjSLWYoNEJkT0UQlcbsUBh9LejSxdANb7OiWr8ANjUSBdSz', '2018-07-16 01:32:30', '2018-07-16 01:52:30');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
